"""
Fit an AAM and produce features and/or landmarks.
"""
import numpy as np
from menpowidgets import visualize_aam, visualize_images, visualize_appearance_model
import menpo.io as mio
from menpo.landmark import labeller
from menpo.landmark import face_ibug_68_to_face_ibug_68_trimesh as ibug_face_66
from menpofit.aam import HolisticAAM, LucasKanadeAAMFitter, WibergInverseCompositional
from menpo.feature import hog as mphog
from menpodetect import load_dlib_frontal_face_detector
import os, glob
import dlib
import menpo
import json
import matplotlib.pyplot as plt
import sys

DATA_PATH = "data\\01M\\si548-frames\\"
IMAGE_TYPE = "*.jpg"

dlib_detector = load_dlib_frontal_face_detector()
detector = dlib.get_frontal_face_detector()

PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"  

MOUTH_OUTLINE_POINTS = list(range(48, 61))  
MOUTH_INNER_POINTS = list(range(61, 68))  
predictor = dlib.shape_predictor(PREDICTOR_PATH) 


class DebugPrinter:
	def __init__(self, dbug=False):
		self.dbug = dbug
	def __call__(self, info):
		if self.dbug:
			print(info)

log = DebugPrinter(False)
SAVE_FILE = "trained_aam.pkl"

MODE = "AAM"
aam = mio.import_pickle(SAVE_FILE)

# TODO: CHECK FITTER PARAMETERS!!
fitter = LucasKanadeAAMFitter(aam, lk_algorithm_cls=WibergInverseCompositional, n_shape=5, n_appearance=30)
log(fitter)		

shape_last_frame = None
landmarks = []
features = []
for image in mio.import_images(DATA_PATH + IMAGE_TYPE):
	if image.n_channels == 3:
		image = image.as_greyscale(mode='luminosity')

	# Fit AAM to Test Image
	initial_bbox = menpo.shape.bounding_box(*image.bounds())
	# TODO: Change max iters.
	if shape_last_frame == None:
		result = fitter.fit_from_bb(image, initial_bbox, max_iters=8)
	else:
		result = fitter.fit_from_shape(image, shape_last_frame, max_iters=8)
	
	# Get the lip landmark positions.
	shape_last_frame = result.final_shape
	fs = result.final_shape.tojson()
	log(fs["landmarks"]["points"])
	landmarks.append(fs["landmarks"]["points"])
	
	log("Shape/Appearance Parameters")
	log(result.shape_parameters[-1])
	log(result.appearance_parameters[-1])
	features.append(np.concatenate((result.shape_parameters[-1], result.appearance_parameters[-1])))
	
log("Processed frames into " + str(len(features)) + "features")

# Save output in Numpy format.
ary_features = np.array(features)
ary_landmarks = np.array(landmarks)

np.save("01M-si548-aam.npy", ary_features)
np.save("01M-si548-landmarks.npy", ary_landmarks)