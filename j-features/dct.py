import cv2
import numpy as np
from lipdetection import *
from pprint import pprint
import glob, os, json, csv
#from progress.bar import Bar
import pickle # protocol 0 is printable ASCII

"""
Configuration Options
N_COEFFS - The number of DCT coefficients to select to form the feature vector.
VIDEO_FILE_P - The path to the video file.
"""
N_COEFFS = 10
VIDEO_FILE_P = "data\\01M\\straightcam"
VIDEO_FILE_TYPE = "*.mp4"

def select_zz_mask(npoints, dim):
	#np.flip(np.tril(np.ones((3,3))), 0)
	n = dim[0] if dim[0] < dim[1] else dim[1]
	def move(i, j):
		if j < (n - 1):
			return max(0, i-1), j+1
		else:
			return i+1, j
	a = [[0] * n for _ in range(n)]
	x, y = 0, 0
	for v in range(npoints):
		a[y][x] = 1
		if (x + y) & 1:
			x, y = move(x, y)
		else:
			y, x = move(y, x)
	return np.matrix(a)
	
def main():
	#sys.exit()
	output = open('output.csv', 'w') # ab
	os.chdir(VIDEO_FILE_P)
	outwriter = csv.writer(output, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
	outwriter.writerow(["file", "frame_number", "feature_vector"])

	fcount = 0
	for file in glob.glob(VIDEO_FILE_TYPE):
		frame_n = 0
		fcount += 1
		# print(file)
		print("File %d/%d" % (fcount, len(glob.glob(VIDEO_FILE_TYPE))), end="\r")
		cap = cv2.VideoCapture(file)
		
		while (True):
			#frame = cv2.imread('face.jpg', cv2.IMREAD_GRAYSCALE)
			ret, frame = cap.read()
			if not ret: # End of File
				break
			frame_n += 1
			#cv2.imshow('window-name', frame)
			frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

			# Extract the lips from the image (see lipdetection.py). 
			rect = get_first_face(frame)
			face = extract_face_image(rect, frame)
			#cv2.imshow("Face", face)
			rect = get_first_face(face)
			min_x, min_y, max_x, max_y = roi_dims(face, rect)
			roi = e_roi(face, (min_x, min_y, max_x, max_y))
			
			roi = cv2.resize(roi, (48, 48))
			#print("ROI Dimensions: ", roi.shape)
			#cv2.imshow("ROI", roi)

			imf = np.float32(roi)/255.0  # float conversion/scale
			dst = cv2.dct(imf)           # the dct
			img = np.uint8(dst)*255.0    # convert back

			#cv2.imwrite('dct.png', img)

			mask = select_zz_mask(N_COEFFS, img.shape)
			#print(mask.shape)
			#print(img.shape)
			#print(mask)
			# a.nonzero
			# np.extract(mask, img)

			#cv2.imshow('image',img)
			#cv2.imshow('mask',mask)
			#cv2.waitKey(0)
			#cv2.destroyAllWindows()

			fvec = np.extract(mask, img).tolist()
			
			outwriter.writerow([file, frame_n, pickle.dumps(fvec, protocol=0)])
			#print("Feature Vector: ", fvec) 

	output.close()	
				
if __name__ == "__main__":
	main()
