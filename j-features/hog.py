import numpy as np  
from lipdetection import *
from skimage.feature import hog
from skimage import data, color, exposure
import matplotlib.pyplot as plt
import os, csv, cv2, dlib, sys, glob, json
import pickle # protocol 0 is printable ASCII

def round_up(n, multiple=16):
	if multiple == 0:
		return n
		
	r = n % multiple
	if r == 0:
		return n
	
	return n + multiple - r

VIDEO_FILE_P = "data\\01M\\straightcam"
VIDEO_FILE_TYPE = "*.mp4"

output = open('output_hog.csv', 'w') # ab
os.chdir(VIDEO_FILE_P)
outwriter = csv.writer(output, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
outwriter.writerow(["file", "frame_number", "feature_vector"])

fcount = 0
for file in glob.glob(VIDEO_FILE_TYPE):
	frame_n = 0
	fcount += 1
	# print(file)
	print("File %d/%d" % (fcount, len(glob.glob(VIDEO_FILE_TYPE))), end="\r")
	cap = cv2.VideoCapture(file)
	
	while (True):
		#frame = cv2.imread('face.jpg', cv2.IMREAD_GRAYSCALE)
		ret, frame = cap.read()
		if not ret: # End of File
			break
		frame_n += 1
		#cv2.imshow('window-name', frame)
		frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		#frame = cv2.imread('face2.jpg', cv2.IMREAD_GRAYSCALE)

		# Extract the lips from the image (see lipdetection.py). 
		rect = get_first_face(frame)
		face = extract_face_image(rect, frame)
		rect = get_first_face(face)
		min_x, min_y, max_x, max_y = roi_dims(face, rect)
		roi = e_roi(face, (min_x, min_y, max_x, max_y))
		roi_wb = cv2.resize(roi, (48, 48))
		
		#cv2.imshow("ROI", roi_wb)
		#cv2.waitKey(0)

		#hog_v, h_image = hog(roi_wb, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(1, 1), block_norm="L2-Hys", visualise=True, transform_sqrt=False, feature_vector=True)
		hog_v = hog(roi_wb, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(1, 1), block_norm="L2-Hys", visualise=False, transform_sqrt=False, feature_vector=True).tolist()
		
		#print("Feature Vector: ", hog_v)
		outwriter.writerow([file, frame_n, pickle.dumps(hog_v, protocol=0)])
		"""
		fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

		ax1.axis('off')
		ax1.imshow(roi_wb, cmap=plt.cm.gray)
		ax1.set_title('Input image')
		ax1.set_adjustable('box-forced')

		# Rescale histogram for better display
		hog_image_rescaled = exposure.rescale_intensity(h_image, in_range=(0, 0.02))

		ax2.axis('off')
		ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
		ax2.set_title('Histogram of Oriented Gradients')
		ax1.set_adjustable('box-forced')
		plt.show()
		"""