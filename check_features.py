import csv
import numpy as np
import pickle
import sys
import ast
import math
from sklearn.naive_bayes import GaussianNB
from sklearn import svm, preprocessing
from sklearn.ensemble import RandomForestClassifier
from random import shuffle
from pprint import pprint
import os
import glob
from scipy import stats
import re
import cv2

# output_dct, output_hog, output_sift
path = os.path.dirname(os.path.realpath(__file__))
path_frames = os.path.join(path, "data", "sequence")
path_gt = os.path.join(path, "data", "frame_by_frame.txt")


class GroundTruth(object):
    def __init__(self, id):
        self.id = id
        self.phonemes = []
        self.visemes = []
        self.fvectors = {}
        self.seq_order = []


config = {
    "features": ["dct", "hog", "sift", "aam", "cnn"],  # dct, hog, sift, aam
    "train": 50,
    "test": 9,
    "folds": 5,
    "classifiers": ["nb", "rf", "svm"]  # nb, svm, rf
}
feature_lenght = {
    "dct": 10,
    "hog": 324,
    "aam": 42,
    "sift": 20,
    "cnn": 2048,
}

p = re.compile('[0-9][0-9][MF]')
total_counter = 0
spkr_counter = 0
ft_spkr_counter = 0

for speaker_dir in sorted(f for f in os.listdir("original") if p.match(f)):
    speaker = os.path.split(speaker_dir)[-1]
    print("--> ", speaker)
    spkr_counter += 1
    for sequence_dir in sorted(glob.glob(os.path.join("original", speaker + '/*'))):
        ft_spkr_counter += 1
        sequence = os.path.split(sequence_dir)[-1].split(".")[0]
        # print("----> ", sequence)
        path = os.path.join("data", "sequence", speaker + "-" + sequence + "-*.npy")
        for feature in config["features"]:
           total_counter += 1
           if not os.path.exists(os.path.join("data", "sequence", speaker + "-" + sequence + "-" + feature + ".npy")):
               print("Missing some features {} for {}/{} {}".format(feature, speaker, sequence, spkr_counter * 98 + ft_spkr_counter))
           else:
               fs = np.load(os.path.join("data", "sequence", speaker + "-" + sequence + "-" + feature + ".npy"))
               fl = feature_lenght[feature]
               for i, fsi in enumerate(fs):
                   if not len(fsi) == fl:
                       print("Feature lenght not correnct. Expected: {}, Actual: {}".format(len(fsi), fl))
        jpgs_res = sorted(glob.glob(os.path.join("data", "resnet", speaker, sequence + "-*.jpg")))
        if jpgs_res :
            for jpg in jpgs_res:
                if not cv2.imread(jpg).shape == (224, 224, 3):
                    print("Some img in resnet is not the correct size for jpg: {}, Expected {}, Actual {}".format(jpg, (224, 224, 3), cv2.imread(jpg).shape))
        else:
            print("This sequence is missing in resnet {}".format(sequence))
        jpgs = sorted(glob.glob(os.path.join("data", "train", speaker, sequence + "-*.jpg")))
        if jpgs:
            for jpg in jpgs:
                if not cv2.imread(jpg, 0).shape == (48, 48):
                    print("Some img in train is not the correct size for jpg: {}, Expected {}, Actual {}".format(jpg, (48, 48, 3), cv2.imread(jpg).shape))
        if not len(jpgs) == len(jpgs_res):
            print("Different amount of frames (dif: {}) for resnet and train for {}/{}".format(len(jpgs) - len(jpgs_res), speaker, sequence))
    if not ft_spkr_counter ==  98:
        print("Expected 98 sequences for a speaker but only got {}".format(ft_spkr_counter))
    ft_spkr_counter = 0
print("Expected 59 Speakers got {}".format(spkr_counter))
print("Expected {} Features files got {} (wrong lengths included)".format(5*59*98, total_counter))

for feature in config["features"]:
    print("Checking ", feature)
    ground_dict = {}
    files = sorted(glob.glob(os.path.join(path_frames, "*" + feature + "*.npy")))
    for file in files:
        # print("Processing: ", file)
        # features = open(os.path.join(path, file))
        spkr, seq, _ = file.split("\\")[-1].split("-")
        spkr = int(os.path.basename(spkr.replace("M", "").replace("F", "")))
        # print("Speaker: ", spkr, "Sequence: ", seq)
        ground_dict[spkr] = GroundTruth(spkr)
        features = np.load(file).tolist()
        if seq not in ground_dict[spkr].fvectors.keys():
            ground_dict[spkr].fvectors[seq] = []
        ground_dict[spkr].fvectors[seq] += features

    groundt = open(path_gt, "r")
    truth_imported = {}
    for line in groundt:
        try:
            gt = line.strip().split(" ")
            spkr = int(os.path.basename(gt[0].replace("M", "").replace("F", "")))
            seq = gt[1]
            if spkr not in truth_imported.keys():
                truth_imported[spkr] = {}
            if seq not in truth_imported[spkr].keys():
                truth_imported[spkr][seq] = {"phonemes": [], "visemes": []}
            if spkr in ground_dict.keys():
                truth_imported[spkr][seq]["phonemes"].append(gt[3])
                truth_imported[spkr][seq]["visemes"].append(gt[4])
        except(ValueError):
            pass  # Skip the header.

    for spkr in ground_dict.keys():
        for seq in ground_dict[spkr].fvectors.keys():
            print("-> Checking speaker " + str(spkr) + "...", end="")
            len_fs = len(ground_dict[spkr].fvectors[seq])
            len_fs_first = len(ground_dict[spkr].fvectors[seq][0])
            len_gt_p = len(truth_imported[spkr][seq]["phonemes"])
            if len_fs == len_gt_p and len_fs_first == feature_lenght[feature]:
                print("OK")
            else:
                print("FAIL")
                print("Feature length: ", len_fs, "Ground truth length: ", len_gt_p)
                print("Failed for speaker: ", str(spkr), "Sequence: ", seq, "Feature: ", feature)
                sys.exit()
