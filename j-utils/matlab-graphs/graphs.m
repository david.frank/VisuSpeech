% https://de.mathworks.com/help/matlab/ref/bar.html
% https://de.mathworks.com/matlabcentral/answers/102220-how-do-i-place-errorbars-on-my-grouped-bar-graph-using-function-errorbar-in-matlab
clss = categorical({'DCT', 'HoG', 'SIFT', 'ASM', 'Random Baseline'});
mean_p = [54.64, 97.96, 97.06, 67.58];
mean_v = [54.76, 74.71, 81.07, 56.28];
merged_series = [mean_p; mean_v].';

error_p = [0.30, 0.09, 0.25, 0.25, 0.0];
error_v = [0.30, 0.77, 1.06, 0.44, 0.0];
merged_error = [error_p; error_v].';

figure;
ax = axes;
bar(merged_series);
hold on;
hline(98.182,'b','Phoneme Baseline');
hline(90,'r','Viseme Baseline');
xticks(ax, [1 2 3 4 5]);
xticklabels(ax, {'DCT', 'HoG', 'SIFT', 'ASM', 'Random Baseline'});
xtickangle(20)
% plot(xlim,[30 30], 'r')
%xlabel ('');

ylabel ('Error Rate (%)');



ngroups = size(merged_series, 1);
nbars = size(merged_series, 2);
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - (groupwidth/2) + (2*i-1) * (groupwidth / (2*nbars));
    errorbar(x, merged_series(:,i), merged_error(:,i), 'k', 'linestyle', 'none');
end

