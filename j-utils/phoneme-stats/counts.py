import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
groundt = open("frame_by_frame.txt", "r")

# Skip header
counts = {}
groundt.readline()
for line in groundt:
	gt = line.strip().split(" ")
	try:
		counts[gt[3]] += 1
	except KeyError:
		counts[gt[3]] = 1
print(counts)
#plt.bar(counts.keys(), counts.values())
#plt.show()

counts.pop('sil', None)
"""
total = 0
for value in counts.values():
	total += value
	
mean = total / len(counts.keys())
print(mean)
"""
serr = stats.sem(list(counts.values()))
print("Standard Error:", serr)

labels, values = zip(*counts.items())
# sort your values in descending order
indSort = np.argsort(values)[::-1]

# rearrange your data
labels = np.array(labels)[indSort]
values = np.array(values)[indSort]

indexes = np.arange(len(labels))

bar_width = 0.35

plt.bar(indexes, values, yerr=serr)

# add labels
plt.xticks(indexes + bar_width, labels)
plt.show()